use player::Player;
use std;
use player::Direction;
use player;
use mio;
use mio::udp;
use mio::udp::UdpSocket;
use mio::buf::ByteBuf;
#[derive(Copy,Clone)]
pub struct Rectangle {
  pub position: (f64,f64), //x,y
  pub width: f64,
  pub height: f64,
}
impl Rectangle {
  pub fn new(x:f64,y:f64,w:f64,h:f64) -> Rectangle { 
    Rectangle {position: (x,y), width: w, height: h}
  }
}
pub fn default_map() -> Vec<Rectangle> { //TODO: Move maps to separate file
  let mut m = vec![];
  m.push(Rectangle::new(2.0,60.0,60.0,2.0));
  m
}
pub struct Map { //This struct contains the map
  players: Vec<Player>, //This is where the player struct will be stored, projectiles will be in here
  pub spectators: Vec<Player>,
  map: Vec<Rectangle>, //The map is made up of rectangles
  kill_y: f64, //Kill players if they go below this y (0 is the top of the main platform)
  spawn_point_1: (f64,f64),
  spawn_point_2: (f64,f64),
  round_time: i64, //nanoseconds
}
impl Map {
  pub fn new() -> Map {
    Map { players: vec![],spectators: vec![], map: default_map(), kill_y: 100.0, spawn_point_1: (5.0,2.0), spawn_point_2: (25.0,2.0), round_time: -3000000000  }
  }
  pub fn send_data(&self, socket: &std::net::UdpSocket) {
    if self.players.len() > 1 {
      let mut s = "u".to_string();
      for p in self.players.iter() {
        s.push_str(f64_to_string(p.position.0).as_ref());
        s.push_str(f64_to_string(p.position.1).as_ref());
      }
      for p in self.players.iter() {
        s.push_str(i16_to_string(p.health).as_ref());
        let mut temporal_string = f64_to_string(p.mana);
        temporal_string.remove(0);
        s.push_str(temporal_string.as_ref());
      }
      let mut buf = s.as_bytes();
      for p in self.spectators.iter() {
        socket.send_to(&buf,&p.addr);
      }
      for p in self.players.iter() {
        socket.send_to(&buf,&p.addr);
      }
    }
  }
  pub fn get_position(&self, src: std::net::SocketAddr, socket: &mio::NonBlock<mio::udp::UdpSocket>) {
    for i in 0..self.spectators.len() {
      if src == self.spectators[i].addr {
        let mut string = std::char::from_digit(i as u32,10).unwrap().to_string();
        string.insert(0,'q');
        let mut buf = mio::buf::ByteBuf::from_slice(string.as_bytes());
        socket.send_to(&mut buf, &src);
      }
    }
    for i in self.players.iter() {
      if src == i.addr {
        let mut buf = mio::buf::ByteBuf::from_slice("qplayer".as_bytes());
        socket.send_to(&mut buf, &src);
      }
    }
  }
  pub fn update_actions(&mut self, src: std::net::SocketAddr, buf: &[u8]) {
    for i in 0 .. 2 {
      if self.players[i].addr == src {
        self.players[i].update_actions(buf);
      }
    }
  }
  pub fn disconnect(&mut self, src: std::net::SocketAddr) {
    for i in 0 .. self.spectators.len() {
      if self.spectators[i].addr == src {
        self.spectators.remove(i);
      }
    }
    for i in 0 .. 2 {
      if self.players[i].addr == src {
        self.players.remove(i);
        self.match_start()
      }
    }
  }
  pub fn change_character(&mut self,src: std::net::SocketAddr, c: player::Character) {
    for i in self.spectators.iter_mut() {
      if i.addr == src {
        i.character = c;
      }
    }
  }
  pub fn update(&mut self) {
    if self.players.len() > 1 {
      self.round_time = self.round_time + (8333333*2);
      println!("{}",self.round_time);
      if self.round_time > 0 {
        for p in self.players.iter_mut() {
          p.change_velocity();
          p.gravity();
          p.map_collide(self.map.clone()); //This method will check to see if the velocity would put the player inside a rectangle and if so sets it to 0
          //p.player_collide(self.players); //Check if the player is colliding with other players, conservation of momentum
        }
        for p in self.players.iter_mut() {
          //p.projectile_update(self.players); //Check to see if projectiles are colliding, move projectiles, damage players 
          p.update(); //Use abilities + attacks
          p.move_player(); //Add player velocity to position
          if p.position.1 > self.kill_y { p.die(); }
        }
        for i in 0..2 {
          let d = self.players[i].deaths;
          if self.players[i].dead == true {self.end_round(d,i); break;}
        }
      }
    } else {
      self.match_start();
    }
  }
  fn match_start(&mut self) {
    if self.spectators.len() > 0 && self.players.len() > 0 {
      self.players[0].deaths = 0;
      self.players.push(self.spectators.remove(0));
      self.players[1].deaths = 0;
      self.round_start();
    } else if self.spectators.len() > 0 {
      self.players.push(self.spectators.remove(0));
    }
  }
  fn round_start(&mut self) {
    self.players[0].position = self.spawn_point_1;
    self.players[1].position = self.spawn_point_2;
    self.players[0].reset(Direction::Right);
    self.players[1].reset(Direction::Left);
    self.round_time = -3000000000;
  }
  fn end_round(&mut self, deaths: u8, p: usize) {
    if deaths == 3 { //This method adds 1 to player deaths and then return it
      self.spectators.push(self.players.remove(p));
      self.match_start();
    } else {
      self.round_start();
    }
  }
}
fn f64_to_string(mut f: f64) -> String {
  let mut s = if f.is_sign_positive() {"+".to_string()} else {"-".to_string()};
  f = f.abs();
  s.push(digit_to_string((f/100.0).floor()));  
  s.push(digit_to_string((f/10.0).floor()));
  s.push(digit_to_string((f.floor())));
  s.push('.');
  s.push(digit_to_string((f*10.0).floor()));
  s.push(digit_to_string((f*10.0).floor()));
  s
}
fn i16_to_string(mut f: i16) -> String {
  let mut s = "".to_string();
  s.push(std::char::from_digit(to_u32(modulo(f/1000)),10).unwrap());
  s.push(std::char::from_digit(to_u32(modulo(f/100)),10).unwrap());
  s.push(std::char::from_digit(to_u32(modulo(f/10)),10).unwrap());
  s.push(std::char::from_digit(to_u32(modulo(f)),10).unwrap());
  s
}
fn to_u32(n: i16) -> u32 {
  if n <= 0 {return 0u32}
  let mut acc = 0u32;
  for i in 0..n {
    acc = acc + 1;
  }
  return acc;
}
#[test]
fn i16_to_u32_test() {
  assert_eq!(to_u32(13i16),13u32);
}
fn modulo(mut n: i16) -> i16 {
  while n >= 10 {
    n = n - 10;
  }
  n
}
fn digit_to_string(mut d: f64) -> char {
  while d >= 10.0 {
    d = d - 10.0;
  }
  match d {
    -0.1...0.1 => '0',
    0.9...1.1 => '1',
    1.9...2.1 => '2',
    2.9...3.1 => '3',
    3.9...4.1 => '4',
    4.9...5.1 => '5',
    5.9...6.1 => '6',
    6.9...7.1 => '7',
    7.9...8.1 => '8',
    8.9...9.1 => '9',
    _ => '0'
  }
}

