//#![feature(core)]
const FPS: f64 = 60.0;
extern crate time;
extern crate mio;
mod world;
mod player;
use world::Map;
//use std::num::ToPrimitive;
pub use mio::udp;
use mio::buf::MutSliceBuf;
use std::net::UdpSocket;
use player::Player;
use std::str;
use std::net::SocketAddr;
use std::str::FromStr;
fn main() {
  println!("Starting server");
  let mut socket = udp::bind(&FromStr::from_str("0.0.0.0:29310").unwrap()).unwrap();
  let mut socket2 = UdpSocket::bind("0.0.0.0:29311").unwrap();
  let mut state = Map::new();
  let mut buf = [0u8; 500];
  loop {
    let start_time = time::precise_time_ns();
    state.update();
    state.send_data(&socket2);
    while time::precise_time_ns() - start_time <= (8333333 * 2) {
      //handle connections
      match socket.recv_from(&mut MutSliceBuf::wrap(&mut buf)) {
        Ok(p) => {
          match p {
            Some(src) => {
              let amt = length(&buf);
              match buf[0] {
                //c,d,e,f,g
                //connect
                99 => {state.spectators.push(Player::new(str::from_utf8(&buf[2..(amt)]).unwrap().to_string(),src,Player::get_character(buf[1]))); println!("{} connected", str::from_utf8(&buf[2..(amt)]).unwrap());},
                //disconnect
                100 => state.disconnect(src),
                101 => state.change_character(src,Player::get_character(buf[1])),
                102 => state.update_actions(src,&buf[1..8]),
                103 => state.get_position(src, &socket),
                _ => break,
              }
              buf = [0; 500]
            },
            None => ()
          }
        },
        Err(e) => (),
      }
    }   
  }
}
fn length(b: &[u8]) -> usize {
  let mut acc = 0;
  for i in b {
    if *i == 0 && acc > 2 { break }
    acc = acc + 1;
  }
  acc
}
