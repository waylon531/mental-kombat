use FPS;
use world::Map;
use world::Rectangle;
use std;
use std::net;

#[derive(PartialEq)]
pub enum Stance {
  Stand,
  Crouch,
}
#[derive(Clone,Copy)]
pub enum Character {
  Einstein,
  Newton,
  Tesla,
}
#[derive(PartialEq,Clone)]
pub enum Direction {
  Left,
  Right,
}
#[test]
fn dir_test() {
  assert!(self::Direction::Left.is_left());
  assert!(self::Direction::Right.is_right());
  assert!(!self::Direction::Left.is_right());
}
impl Direction {
  pub fn is_left(&self) -> bool {
    if *self == Direction::Left {true} else {false} 
  }
  pub fn is_right(&self) -> bool {
    if *self == Direction::Right {true} else {false}
  }
}
pub struct Player {
  pub health: i16,
  pub mana: f64,
  pub name: String,
  pub addr: std::net::SocketAddr,
  pub position: (f64,f64), //Center
  velocity: (f64,f64), 
  //stance: self::Stance,
  pub character: self::Character,
  pub deaths: u8,
  pub dead: bool,
  pub facing: self::Direction,
  //Now for desired actions
  jump: bool,
  movement_direction: Option<self::Direction>,
  abilities: [bool; 4],
  basic_attack: bool,
  power_attack: bool
} 
impl Player {
  pub fn new(n: String, a: std::net::SocketAddr, c: self::Character) -> Player {
    Player { health: 0, mana: 0.0, name: n, addr: a, position: (0.0,0.0), velocity: (0.0,0.0), character: c, deaths: 0, dead: false, facing: self::Direction::Left, jump: false, movement_direction: None, abilities: [false; 4], basic_attack: false, power_attack: false }
  }
  pub fn update_actions(&mut self, buf: &[u8]) {
    if buf[0] == 0 {self.jump = false;} else {self.jump = true;}
    if buf[1] == 0 {self.facing = self::Direction::Left;} else {self.facing = self::Direction::Right;}
    for i in 2 .. 6 {
      if buf[i] == 0 {
        self.abilities[i-2] = false;
      } else {
        self.abilities[i-2] = true;
      }
    }
    if buf[6] == 0 {self.basic_attack = false;} else {self.basic_attack = true;}
    if buf[7] == 0 {self.power_attack = false;} else {self.power_attack = true;}
  }
  pub fn get_character(c: u8)-> self::Character {
    match c {
      0 => self::Character::Einstein,
      1 => self::Character::Tesla,
      2 => self::Character::Newton,
      _ => self::Character::Einstein,
    }
  }
  pub fn update(&mut self) {
    self.mana = self.mana + (10.0/FPS);
  }
  pub fn reset(&mut self, f: self::Direction) {
    self.mana = 0.0;
    self.velocity = (0.0,0.0);
    self.facing = f;
    self.velocity = (0.0,0.0);
    self.dead = false;
    self.jump = false;
    self.movement_direction = None;
    self.abilities = [false; 4];
    self.basic_attack = false;
    self.power_attack = false;
    match self.character {
      _ => self.health = 1000,
    }
  }
  pub fn change_velocity(&mut self) {
    match self.movement_direction.clone() {
      None => (),
      Some(dir) => if dir.is_left() { self.velocity.0 = self.velocity.0 - ( 1.0 / FPS ) } 
                      else if dir.is_right() { self.velocity.0 = self.velocity.0 + (1.0 / FPS)}
    }
  }
  pub fn move_player(&mut self) {
    self.position.0 = self.position.0 + self.velocity.0;
    self.position.1 = self.position.1 + self.velocity.1;
  }
  pub fn die(&mut self) {
    self.deaths = self.deaths+1;
    self.dead = true;
  }
  fn get_dimensions(&self) -> (f64,f64) { //height, width
    match self.character {
      _ => (4.67, 2.9) //In 100s of pixels, coordinates are from top left
    }
  }
  pub fn gravity(&mut self) {
    self.velocity.1 = self.velocity.1 + (10.0/FPS);
    println!("{}",self.velocity.1)
  }
  pub fn map_collide(&mut self, rv: Vec<Rectangle>) {
    let (x,y) = self.position;
    let (h,w) = self.get_dimensions();
    for r in rv {
      let (rx,ry) = r.position;
      if x < rx + r.width && x + w > rx && y < ry +r.height && h + y > ry {
        //Check to see if the rectangle is in the same direction as velocity
        //The difference between the top or bottom edge will be very small if they are the ones colliding
        println!("collision!");
        if (y  - (ry - r.height)).abs() < 0.15 && self.velocity.1 > 0.0 { //top of player - bottom of map rect
          self.velocity.1 = 0.0;
        } else if (ry - (y - h)).abs() < 0.15 && self.velocity.1 < 0.0 {
          self.velocity.1 = 0.0;
          if self.jump {
            self.velocity.1 = -50.0;
          }
        }
        if (x -(rx -r.width)).abs() < 0.15 && self.velocity.0 > 0.0 {
          self.velocity.0 = 0.0;
        }
        if (rx - (x - w)).abs() < 0.15 && self.velocity.0 < 0.0 {
          self.velocity.0 = 0.0;
        }
      }
    }
  }
}
